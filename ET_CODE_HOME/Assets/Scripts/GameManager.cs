﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private bool running = true;
    private int pickUps;
    private static int currentSceneIndex;

    public GameObject levelComplete;

    public int getSceneIndex() {
        return currentSceneIndex;
    }
    public void Start()
    {
        if(SceneManager.GetActiveScene().buildIndex == 10) return;
        currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
    }

    public void Update()
    {
        pickUps = GameObject.FindGameObjectsWithTag("Pick Up").Length;
        if(pickUps == 0 && SceneManager.GetActiveScene().buildIndex != 10) {
            running = false;
            LevelComplete();
        }
    }

    public void LevelComplete() 
    {
        levelComplete.SetActive(true);
        Invoke("NextScene", 2f);
    }

    public void NextScene() 
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    
    public void EndGame()
    {
        if(running) {
            running = false;
            SceneManager.LoadScene(10);
        }
    }

    void Restart()
    {
        SceneManager.LoadScene(currentSceneIndex);
    }
}
