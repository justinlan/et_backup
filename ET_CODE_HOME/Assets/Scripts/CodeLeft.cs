﻿using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class CodeLeft : MonoBehaviour
{
    private int[] limits = new int[] {0,2,4,4,7,6,7,6,8,6,0};
    private int limit;

    private void Start()
    {
        limit = limits[SceneManager.GetActiveScene().buildIndex];
        GetComponentInChildren<TMP_Text>().text = limit.ToString();
    }
    
    public void Increase()
    {
        limit++;
        GetComponentInChildren<TMP_Text>().text = limit.ToString();
    }

    public void Increase(int n)
    {
        limit += n;
        GetComponentInChildren<TMP_Text>().text = limit.ToString();
    }

    public void Decrease()
    {
        limit--;
        GetComponentInChildren<TMP_Text>().text = limit.ToString();
    }

    public void Decrease(int n)
    {
        limit -= n;
        GetComponentInChildren<TMP_Text>().text = limit.ToString();
    }

    public bool RunOut()
    {
        if(limit == 0) return true;
        return false;
    }
}
