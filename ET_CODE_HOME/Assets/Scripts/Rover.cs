﻿using UnityEngine;

/* This class is responsible for the functions of the rover */
public class Rover
{
    private GameObject rover;
    private Transform trans;
    private float distance;
    private float rotation;
    private float speed;
    private float rotspeed;
    private float range;
    private bool finished;
    private bool hitBox;
    private RaycastHit hitInfo;
    private Vector3 posBox;

    public Rover(GameObject rover)
    {
        this.rover = rover;
        this.trans = rover.GetComponent<Transform>();
        distance = 0f;
        rotation = 0f;
        speed = 2f;
        rotspeed = 200f;
        range = 1f;
        finished = false;
    }

    public void push()
    {
        if(checkForBox() == false) return;
        if(hitInfo.transform.tag == "Box") {                
            if(distance + (speed * Time.deltaTime) < 1) {
                hitInfo.transform.Translate(trans.forward * speed * Time.deltaTime, Space.World);
                distance += speed * Time.deltaTime;
            } else {
                hitInfo.transform.Translate(trans.forward - (hitInfo.transform.position - posBox), Space.World);
                finished = true;
                hitBox = false;
                distance = 0;
            }
        }
    }

    public bool checkForBox()
    {
        if(!hitBox) {
            if(Physics.Raycast(trans.position, trans.forward, out hitInfo, range)) {
                hitBox = true;
                posBox = hitInfo.transform.position;
            } else {
                hitBox = false;
                finished = true;
                return false;
            }
        }
        return true;
    }

    // Moves GameObject 1 unit along its local z-axis
    public void forward()
    {
        if(distance + (speed * Time.deltaTime) < 1) {
            trans.Translate(Vector3.forward * speed * Time.deltaTime);
            distance += speed * Time.deltaTime;
        } else {
            trans.Translate(new Vector3(0, 0, 1-distance));
            finished = true;
            distance = 0;
        }
    }

    // Rotates GameObject 90 degrees anticlockwise about world space y-axis
    public void left()
    {
    	if(rotation + (rotspeed * Time.deltaTime) < 90) {
    	    trans.Rotate(new Vector3(0, -rotspeed, 0) * Time.deltaTime);
            rotation += rotspeed * Time.deltaTime;
    	} else {
            trans.Rotate(new Vector3(0, rotation-90, 0));
            finished = true;
            rotation = 0;
    	}	
    }

    // Rotates GameObject 90 degrees clockwise about world space y-axis
    public void right()
    {
    	if(rotation + (rotspeed * Time.deltaTime) < 90) {
            trans.Rotate(new Vector3(0, rotspeed, 0) * Time.deltaTime);
            rotation += rotspeed * Time.deltaTime;
    	} else {
            trans.Rotate(new Vector3(0, 90-rotation, 0));
            finished = true;
            rotation = 0;
    	}	
    }

    // Getter for finished state
    public bool getFinished()
    {
        return finished;
    }

    // Setter for finished state
    public void setFinished(bool finished)
    {
        this.finished = finished;
    }
}
