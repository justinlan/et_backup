﻿using UnityEngine;

public class FuelCell : MonoBehaviour
{
    private Animator anim;
    private AudioSource source;

    void Start()
    {
        anim = GetComponent<Animator>();
        source = GameObject.FindWithTag("Rover").GetComponent<AudioSource>();
    }

    void Update()
    {
        transform.Rotate(new Vector3(0, 60, 0) * Time.deltaTime, Space.World);
    }

    void Destroy()
    {
        Destroy(this.gameObject);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") || other.gameObject.CompareTag("Rover"))
        {
            source.Play();
            anim.Play("GotFuelcell"); 
            Invoke("Destroy",.3f);          
        }
    }
}
