﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class Terminal : MonoBehaviour, IDropHandler
{
    private Rover rover;
    private Button run;
    private List<int> commands = new List<int>();
    private int pc = 0;
    private bool runProg = false;
    private string scene;
    public GameObject arrow;

    private void Start()
    {
        rover = new Rover(GameObject.Find("Rover"));
        run = GameObject.Find("Run").GetComponent<Button>();
        run.onClick.AddListener(RunOnClick);
        scene = SceneManager.GetActiveScene().name;
        if(scene == "Level1") arrow.SetActive(true);
    }

    void LateUpdate()
    {
        if(runProg) {
            RunProgram();
            if(rover.getFinished()) {
                rover.setFinished(false);
                pc++;
                if(pc == commands.Count) {
                    runProg = false;
                    Invoke("EndGame", 2f);
                }
            } 
        }
    }

    public void EndGame()
    {
        int pickUps = GameObject.FindGameObjectsWithTag("Pick Up").Length;
        if(pickUps != 0) SceneManager.LoadScene(10);
    }

    void RunOnClick()
    {
        if(!runProg) {
            commands.Clear();
            Compile(1, this.gameObject);
        }  
    }

    void Compile(int repeat, GameObject parent)
    {
        for(int i=0; i < repeat; i++) {
            int children = parent.transform.childCount;
            for(int j=0; j < children ; j++) {
                Transform child = parent.transform.GetChild(j);
                if(child.tag == "Forward") commands.Add(0);
                if(child.tag == "Left") commands.Add(1);
                if(child.tag == "Right") commands.Add(2);
                if(child.tag == "Loop") {
                    string arg = child.GetComponentInChildren<TMP_InputField>().text;
                    if(string.IsNullOrEmpty(arg) == false) Compile(int.Parse(arg), child.gameObject);
                }
            }
        }
        if(commands.Count > 0) {
            run.onClick.RemoveListener(RunOnClick);
            runProg = true;
        }
    }

    void RunProgram()
    {
        switch (commands[pc]) {
            case 0:
                rover.forward();
                break;
            case 1:
                rover.left();
                break;
            case 2:
                rover.right();
                break;
            default:
                runProg = false;
                break;
        }
    }

    public void OnDrop(PointerEventData eventData) 
    {
        if(arrow.activeSelf) arrow.SetActive(false);
    }
}