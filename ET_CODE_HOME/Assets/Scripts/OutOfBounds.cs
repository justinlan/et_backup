﻿using UnityEngine;

public class OutOfBounds : MonoBehaviour
{
    private Rigidbody rb;

    void Start() {
        rb = gameObject.GetComponent<Rigidbody>();
    }

    void FixedUpdate() {
        if(rb.position.y < -0.5) {
            FindObjectOfType<GameManager>().EndGame();
        }
    }
}
