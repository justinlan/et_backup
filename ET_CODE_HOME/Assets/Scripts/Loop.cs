﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Loop : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    private Vector2 offset;
    private Transform parentToReturnTo = null;
    private GameObject dropmarker = null;
    private GameObject codeLeft;
    private CodeLeft counter;

    public void Start()
    {
        codeLeft = GameObject.Find("CodeLeft");
        counter = (CodeLeft)codeLeft.GetComponent(typeof(CodeLeft));
    }
    
    private void Update() 
    {
        handleFiller();
        fitChildren();
    }

    private void handleFiller()
    {
        if(transform.childCount < 3) {
            transform.Find("Filler").gameObject.SetActive(true);
        }
        else {
            transform.Find("Filler").gameObject.SetActive(false);
        }
    }

    private void fitChildren() 
    {
        RectTransform rt = this.GetComponent<RectTransform>();
        float maxWidth = 0;
        float width = 100;
        float height = 10;
        for(int i=0; i<transform.childCount; i++) {
            if(this.transform.GetChild(i).gameObject.activeSelf) {
                Vector2 childSize = transform.GetChild(i).GetComponent<RectTransform>().sizeDelta;
                height += childSize.y;
            }
        } 
        rt.sizeDelta = new Vector2( width + maxWidth, height);
    }

    private int countCode(GameObject parent) 
    {
        int code = 1;
        int children = parent.transform.childCount;
        for(int i=0; i<children; i++) {
            string t = parent.transform.GetChild(i).gameObject.tag;
            if (t == "Forward" || t == "Left" || t == "Right") {
                code++;
            } else if (t == "Loop") {
                code += countCode(parent.transform.GetChild(i).gameObject);
            }
        }
        return code;
    }

    // All interface methods must be public

    public void OnPointerDown(PointerEventData eventData)
    {
        offset = this.transform.position;
        offset -= eventData.position;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        counter.Increase(countCode(this.gameObject));
        this.transform.SetParent(GameObject.Find("Canvas").transform);
        GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

   public void OnDrag(PointerEventData eventData)
    {
        if(dropmarker == null) {
            dropmarker = Instantiate(GameObject.Find("Dropmarker"), GameObject.Find("Canvas").transform);
        }
        this.transform.position = (eventData.position + offset);
        if(setDropmarker(eventData)) Destroy(dropmarker);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        bool destroy = true;
        List<RaycastResult> raycastResults = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventData, raycastResults);
        if(raycastResults.Count != 0) {
            for(int i=0; i<raycastResults.Count; i++) {
                if(raycastResults[i].gameObject.name == "Terminal") {
                    counter.Decrease(countCode(this.gameObject));
                    this.transform.SetParent(parentToReturnTo);
                    this.transform.SetSiblingIndex(dropmarker.transform.GetSiblingIndex()); 
                    GetComponent<CanvasGroup>().blocksRaycasts = true;
                    destroy = false; 
                }
            }
        } 
        if(destroy) Destroy(this.gameObject);
        Destroy(dropmarker);
    }

    private bool setDropmarker(PointerEventData eventData)
    {
        int start = 0;
        int siblingIndex;
        List<RaycastResult> raycastResults = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventData, raycastResults);
        if(raycastResults.Count != 0) {
            for(int i=0; i<raycastResults.Count; i++) {
                if(raycastResults[i].gameObject.tag == "Terminal" || raycastResults[i].gameObject.tag == "Loop") {
                    start = changeDropmarker(raycastResults[i].gameObject.transform.tag);
                    siblingIndex = raycastResults[i].gameObject.transform.childCount;
                    parentToReturnTo = raycastResults[i].gameObject.transform;
                    dropmarker.transform.SetParent(raycastResults[i].gameObject.transform);
                    for(int j = start; j<raycastResults[i].gameObject.transform.childCount; j++) {
                        if(eventData.position.y > raycastResults[i].gameObject.transform.GetChild(j).position.y) {
                            siblingIndex = j;
                            if(dropmarker.transform.GetSiblingIndex() < siblingIndex) {
                                siblingIndex--;
                            }
                            break;
                        }
                    }
                    dropmarker.transform.SetSiblingIndex(siblingIndex);
                    return false;
                }
            }
        }
        return true;
    }

    private int changeDropmarker(string parent) {
        if(parent == "Loop") {
            dropmarker.GetComponent<RectTransform>().sizeDelta = new Vector2(85, 25);
            dropmarker.GetComponent<Image>().color = new Vector4(255, 255, 255, 50f/255f);
            return 1;
        }
        dropmarker.GetComponent<RectTransform>().sizeDelta = new Vector2(100, 25);
        dropmarker.GetComponent<Image>().color = new Vector4(255, 255, 255, 0);
        return 0;
    }

}
