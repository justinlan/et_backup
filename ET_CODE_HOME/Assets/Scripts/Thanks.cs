﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Thanks : MonoBehaviour
{
    private bool end = false;
    private Rigidbody rb;
    public AudioSource source;
    
    void Start() {
        rb = gameObject.GetComponent<Rigidbody>();
    }

    void FixedUpdate() {
        if(rb.position.y < -0.5 && !end) {
            end = true;
            source.Play();
            Invoke("StartMenu", 2f);
        }
    }

    void StartMenu() 
    {
        SceneManager.LoadScene(0);
    }
}
