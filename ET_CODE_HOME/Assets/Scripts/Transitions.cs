﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Transitions : MonoBehaviour
{

  private Rigidbody rb;
  private bool running = true;
  int mainMenuIndex = 0;
  int gameOverIndex = 10;

  public void LoadLastScene()
  {
     SceneManager.LoadScene(FindObjectOfType<GameManager>().getSceneIndex());
  }

  public void LoadByIndex(int sceneIndex){
    SceneManager.LoadScene(sceneIndex);
  }

  public void LoadMainMenu(){
    LoadByIndex(mainMenuIndex);
  }

  public void LoadGameOver(){
    LoadByIndex(gameOverIndex);
  }

  public void CheckEndGame()
  {
      if(running){
          running = false;
          LoadGameOver();
      }
  }
}
