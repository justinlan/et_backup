﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MenuItem : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    private Vector2 offset;
    private Transform parentToReturnTo = null;
    private GameObject placeholder = null;
    private GameObject dropmarker = null;
    private GameObject codeLeft;
    private CodeLeft counter;

    private void Start() 
    {
        parentToReturnTo = this.transform.parent;
        codeLeft = GameObject.Find("CodeLeft");
        counter = (CodeLeft)codeLeft.GetComponent(typeof(CodeLeft));
    }
    
    // All interface methods must be public

    public void OnPointerDown(PointerEventData eventData)
    {
        offset = this.transform.position;
        offset -= eventData.position;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if(counter.RunOut()) return;
        placeholder = new GameObject();
        placeholder.name = "Placeholder";
        placeholder.transform.SetParent(this.transform.parent);
        RectTransform rt = placeholder.AddComponent<RectTransform>();        
        rt.sizeDelta = this.GetComponent<RectTransform>().sizeDelta;
        placeholder.transform.SetSiblingIndex(this.transform.GetSiblingIndex());
        this.transform.SetParent(GameObject.Find("Canvas").transform);
        GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        if(counter.RunOut()) return;
        if(dropmarker == null) {
            dropmarker = Instantiate(GameObject.Find("Dropmarker"), GameObject.Find("Canvas").transform);
        }
        this.transform.position = (eventData.position + offset);
        if(setDropmarker(eventData)) Destroy(dropmarker);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if(counter.RunOut()) return;
        this.transform.SetParent(parentToReturnTo);
        this.transform.SetSiblingIndex(placeholder.transform.GetSiblingIndex()); 
        List<RaycastResult> raycastResults = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventData, raycastResults);
        if(raycastResults.Count != 0) {
            for(int i=0; i<raycastResults.Count; i++) {
                GameObject go = raycastResults[i].gameObject;
                if(go.tag == "Terminal" || go.tag == "Loop") {
                    AddCode(eventData, go);
                    counter.Decrease();
                    break;
                }
            }
        } 
        GetComponent<CanvasGroup>().blocksRaycasts = true;
        Destroy(placeholder);
        Destroy(dropmarker);
    }

    void AddCode(PointerEventData eventData, GameObject parent)
    {
        GameObject newObj = null;
        string item = eventData.pointerDrag.name + "Code";
        if(item != null) {
            newObj = Instantiate(GameObject.Find(item), parent.transform);
        }
        if(newObj != null) newObj.transform.SetSiblingIndex(getIndex());
    }

    public int getIndex() 
    {
        if(dropmarker != null) return dropmarker.transform.GetSiblingIndex();
        else return 0;
    }

    private bool setDropmarker(PointerEventData eventData)
    {
        int start = 0;
        int siblingIndex;
        List<RaycastResult> raycastResults = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventData, raycastResults);
        if(raycastResults.Count != 0) {
            for(int i=0; i<raycastResults.Count; i++) {
                if(raycastResults[i].gameObject.tag == "Terminal" || raycastResults[i].gameObject.tag == "Loop") {
                    start = changeDropmarker(raycastResults[i].gameObject.transform.tag);
                    siblingIndex = raycastResults[i].gameObject.transform.childCount;
                    dropmarker.transform.SetParent(raycastResults[i].gameObject.transform);
                    for(int j = start; j<raycastResults[i].gameObject.transform.childCount; j++) {
                        if(eventData.position.y > raycastResults[i].gameObject.transform.GetChild(j).position.y) {
                            siblingIndex = j;
                            if(dropmarker.transform.GetSiblingIndex() < siblingIndex) {
                                siblingIndex--;
                            }
                            break;
                        }
                    }
                    dropmarker.transform.SetSiblingIndex(siblingIndex);
                    return false;
                }
            }
        }
        return true;
    }

    private int changeDropmarker(string parent) {
        if(parent == "Loop") {
            dropmarker.GetComponent<RectTransform>().sizeDelta = new Vector2(85, 25);
            dropmarker.GetComponent<Image>().color = new Vector4(255, 255, 255, 50f/255f);
            return 1;
        }
        dropmarker.GetComponent<RectTransform>().sizeDelta = new Vector2(100, 25);
        dropmarker.GetComponent<Image>().color = new Vector4(255, 255, 255, 0);
        return 0;
    }
}
